var jungleRun = function()
{
  this.canvas = document.getElementById('game-canvas'),
  this.context = this.canvas.getContext('2d'),
  this.fpsElement = document.getElementById('fps'),
  this.livesElement = document.getElementById('lives'),
  this.toastElement = document.getElementById('toast'),
  this.instructionElement = document.getElementById('instructions'),
  this.copyrightElement = document.getElementById('copyright'),
  this.scoreElement = document.getElementById('score'),
  this.soundAndMusicElement = document.getElementById('sound-and-music'),
  this.loadingElement = document.getElementById('loading'),
  this.loadingTitleElement = document.getElementById('loading-title'),
  this.loadingAnimatedGIFElement = document.getElementById('loading-animated-gif'),
  this.gameOverElement = document.getElementById('gameOver'),
  this.gameLevelElement = document.getElementById('level'),
  this.highScoreElement = document.getElementById('high-score'),
  this.pausedElement = document.getElementById('paused'),
  this.lowFramesElement = document.getElementById('low-frames'),  
  this.gameCompleteElement = document.getElementById('complete'),
  this.gameCompleteHSElement = document.getElementById('paused-info-highScore'),
//this.context.scale(1.1, 1.1);+  

 //Music
this.musicElement = document.getElementById('jungleRun-music'); 
this.musicElement.volume = 0.1;
this.musicCheckboxElement = document.getElementById('music-checkbox');
this.musicOn = this.musicCheckboxElement.checked
//Sound
this.soundCheckboxElement = document.getElementById('sound-checkbox');
this.audioSprites = document.getElementById('jungleRun-audio-sprites');
this.soundOn = this.soundCheckboxElement.checked;

 //score
  this.score = 0;
  this.highScore = 0;
  this.gameLevel = 1;

//booleans
this.isLosingLife = false;
this.isAscendingBool = false;

//lives
this.lives = 3;
this.livesElement.innerHTML = 'lives: ' + 3;

// Mobile............................................................

   this.mobileInstructionsVisible = false;

   this.mobileStartToast = 
      document.getElementById('jungleRun-mobile-start-toast');

   this.mobileWelcomeToast = 
      document.getElementById('jungleRun-mobile-welcome-toast');

   this.welcomeStartLink = 
      document.getElementById('jungleRun-welcome-start-link');

   this.showHowLink = 
      document.getElementById('jungleRun-show-how-link');

   this.mobileStartLink = 
      document.getElementById('jungleRun-mobile-start-link');

  //Time
  this.playing = true;
  this.timeSystem = new TimeSystem();
  this.timeRate = 1.0; //1.0 is normal time, 0.5 1/2 speed

  //Pixels and meters
  this.CANVAS_WIDTH_IN_METRES = 13,
  this.PIXELS_PER_METRE = this.canvas.width/this.CANVAS_WIDTH_IN_METRES;

  //Gravity
  this.GRAVITY_FORCE = 9.81;

  //Constants
  this.RIGHT = 1,
  this.LEFT = 2,
  this.BUTTON_PACE_VELOCITY = 80,
  this.RUNNER_LEFT = 50,
  this.PLATFORM_HEIGHT = 20,
  this.PLATFORM_STROKE_WIDTH = 2,
  this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)',
  this.gameFinish = false;
  this.gamePaused = true;

  this.RUNNER_EXPOSION_DURATION = 500,
  this.BAD_GUYS_EXPLOSION_DURATION = 1500,

  this.STARTING_RUNNER_TRACK = 4,

  //there will be 12 baselines 
  this.TRACK_1_BASELINE = 422,
  this.TRACK_2_BASELINE = 420,
  this.TRACK_3_BASELINE = 404,
  this.TRACK_4_BASELINE = 393,
  this.TRACK_5_BASELINE = 373,
  this.TRACK_6_BASELINE = 343,
  this.TRACK_7_BASELINE = 285,
  this.TRACK_8_BASELINE = 277,
  this.TRACK_9_BASELINE = 243,
  this.TRACK_10_BASELINE = 236,
  this.TRACK_11_BASELINE = 220,
  this.TRACK_12_BASELINE = 211,
  this.TRACK_13_BASELINE = 188,

  this.SHORT_DELAY = 50,
  this.OPAQUE = 1.0,
  this.TRANSPARENT = 0,

  //Game states
  this.paused = false,
  this.PAUSED_CHECK_INTERVAL = 200, //time in milliseconds
  this.pauseStartTime,
  this.windowHasFocus = true,
  this.countDownInProgress = false,
  this.gameStarted = false,
  this.playing = false,
  //Images
  this.background = new Image(),
  this.platformImages = new Image(),
  this.runner = new Image(),
  this.spritesheet = new Image(),
  //this.loadingAnimatedGIFElement = new Image();

  //Time
  this.lastAnimationFrameTime = 0,
  this.lastFpsUpdateTime = 0,
  this.lastScoreUpdateTime = 0,
  this.fps = 60;

  //Scrolling
  this.STARTING_BACKGROUND_OFFSET = 0,
  this.STARTING_BACKGROUND_VELOCITY = 0,
  this.BACKGROUND_VELOCITY = 20,
  this.STARTING_PLATFORM_OFFSET = 0,
  this.PLATFORM_VELOCITY_MULTIPLER = 4.35,
  this.STARTING_SPRITE_OFFSET = 0,

  //Animation
  this.RUN_ANIMATION_RATE = 14,


  //Translation offsets
  this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
  this.spriteOffset = this.STARTING_SPRITE_OFFSET,
  this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
  this.platformOffset = this.STARTING_PLATFORM_OFFSET,
  this.platformVelocity;

  //Key codes
  this.KEY_D = 68,
  this.KEY_LEFT = 37,
  this.KEY_K = 75,
  this.KEY_RIGHT = 39,
  this.KEY_P = 80,

 // Sprite sheet cells................................................

   this.RUNNER_CELLS_WIDTH = 21; // pixels
   this.RUNNER_CELLS_HEIGHT = 24;

  

   this.BEE_CELLS_HEIGHT = 50;
   this.BEE_CELLS_WIDTH  = 50;

   this.BUTTON_CELLS_HEIGHT  = 20;
   this.BUTTON_CELLS_WIDTH   = 31;

   this.COIN_CELLS_HEIGHT = 30;
   this.COIN_CELLS_WIDTH  = 30;
   
   this.EXPLOSION_CELLS_HEIGHT = 62;

   this.RUBY_CELLS_HEIGHT = 30;
   this.RUBY_CELLS_WIDTH = 35;

   this.ARCHER_CELLS_WIDTH =  27;
   this.ARCHER_CELLS_HEIGHT = 31;

   this.ARCHER_ARROW_CELLS_HEIGHT = 20;
   this.ARCHER_ARROW_CELLS_WIDTH  = 20;

   this.BADDIE_CELLS_HEIGHT = 14;
   this.BADDIE_CELLS_WIDTH  = 14;
   this.BADDIE_PACE_VELOCITY = 40;

this.archerCells = [
{ left: 293,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
{ left: 262,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 232,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 201,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
   { left: 167,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 133,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 101,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 72,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
  { left: 42,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
{ left: 8,   top: 585, 
  width: this.ARCHER_CELLS_WIDTH, height: this.ARCHER_CELLS_HEIGHT },
];

  

  
   
   this.beeCells = [
      { left: 5,   top: 234, width: this.BEE_CELLS_WIDTH,
                            height: this.BEE_CELLS_HEIGHT },

      { left: 75,  top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT },

      { left: 145, top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT }
   ];
   
   this.blueCoinCells = [
      { left: 5, top: 540, width: this.COIN_CELLS_WIDTH, 
                           height: this.COIN_CELLS_HEIGHT },

      { left: 5 + this.COIN_CELLS_WIDTH, top: 540,
        width: this.COIN_CELLS_WIDTH, 
        height: this.COIN_CELLS_HEIGHT }
   ];

   this.explosionCells = [
      { left: 3,   top: 48, 
        width: 52, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 63,  top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 146, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 308, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 392, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 473, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT }
   ];

   // Sprite sheet cells................................................

   this.blueButtonCells = [
      { left: 10,   top: 192, width: this.BUTTON_CELLS_WIDTH,
                            height: this.BUTTON_CELLS_HEIGHT },

      { left: 53,  top: 192, width: this.BUTTON_CELLS_WIDTH, 
                            height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.goldCoinCells = [
      { left: 65, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 96, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 128, top: 540, width: this.COIN_CELLS_WIDTH, 
                             height: this.COIN_CELLS_HEIGHT },
   ];


   this.rubyCells = [
      { left: 3,   top: 138, width: this.RUBY_CELLS_WIDTH,
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 39,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 76,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 112, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 148, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT }
   ];

   this.runnerCellsRight = [
      { left: 261, top: 477, 
        width: 21, height: this.RUNNER_CELLS_HEIGHT },

      { left: 293, top: 476, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT },

      { left: 325, top: 477, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT },

      { left: 357, top: 476, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT }
   ],

   this.runnerCellsLeft = [
      { left: 262,   top: 508, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT },

      { left: 294,  top: 509, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT },

      { left: 326, top: 508, 
         width: 19, height: this.RUNNER_CELLS_HEIGHT },

      { left: 358, top: 509, 
         width: 21, height: this.RUNNER_CELLS_HEIGHT }
   ],

   this.archerArrowCells = [
      { left: 14, top:624, width: 13, height: 7 },
      { left: 14, top: 624, width: 13, height: 7 }
   ];

   this.baddieCells = [
   //green baddie
      {left: 536, top:395, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 555, top:395, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 570, top:396, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 585, top:396, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 601, top:395, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 617, top:396, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},


   ]; 

   this.baddieCells2 = [
   //blue baddie
      {left: 539, top:334, width: this.BADDIE_CELLS_WIDTH, height: 11},

      {left: 555, top:333, width: this.BADDIE_CELLS_WIDTH, height: 11},

      {left: 571, top:334, width: this.BADDIE_CELLS_WIDTH, height: 11},

      {left: 587, top:334, width: this.BADDIE_CELLS_WIDTH, height: 11},

      {left: 603, top:333, width: this.BADDIE_CELLS_WIDTH, height: 11},

      {left: 619, top:334, width: this.BADDIE_CELLS_WIDTH, height: 11},


   ]; 

   this.baddieCells3 = [
   //red baddie
      {left: 539, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 555, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 571, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 587, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 603, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},

      {left: 619, top:362, width: this.BADDIE_CELLS_WIDTH, height: this.BADDIE_CELLS_HEIGHT},


   ]; 

   // Sprite data.......................................................

   
   this.beeData = [
      
      { left: 710,  
         top: 306 },

      { left: 1506, top: 240 },
      { left: 1880, top: 345 },
      { left: 2250, top: 275 },
      

   ];
   
   this.buttonData = [
       { platformIndex: 20 },
       
   ];

   this.coinData = [

         { left: 270,  
         top: this.TRACK_6_BASELINE - this.COIN_CELLS_HEIGHT }, 
         { left: 369,  
         top: 193 - this.COIN_CELLS_HEIGHT }, 
         { left: 1014,  
         top: this.TRACK_13_BASELINE - this.COIN_CELLS_HEIGHT }, 
         { left: 1392,  
         top: this.TRACK_13_BASELINE}, 
         { left: 1984,  
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT },
         { left: 2214,  
         top: 290 - this.COIN_CELLS_HEIGHT },
   ];  



  this.platformData = [
  //Stage 1
    {
      left: 25,
      width: 230,
      height: this.PLATFORM_HEIGHT,
      opacity: 0,
      track: 4,
      canJumpThrough: false,
      
    },
    {
      left: 263,
      width: 66,
      height: this.PLATFORM_HEIGHT,
      opacity: 0,
      track: 6,
      canJumpThrough: false,
      
    },
    {
      left: 363,
      width: 32,
      height: this.PLATFORM_HEIGHT,
      opacity: 0,
      track: 4,
      canJumpThrough: false,
      
    },
    {
      left: 449,
      width: 138,
      height: 80,
      opacity: 0,
      track: 5,
      canJumpThrough: false,
      
    },
    //stage 2
    {
      left: 619,
      width: 132,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 4,
      canJumpThrough: false,
      
    },
    {
      left: 550,
      width: 33,
      height: 12,
      opacity: 0.0,
      track: 6,
      canJumpThrough: true,
      
    },
    {
      left: 486,
      width: 65,
      height: 12,
      opacity: 0.0,
      track: 8,
      canJumpThrough: true,
      
    },

    {
      left: 396,
      width: 65,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 9,
      canJumpThrough: false,
      
    },

    {
      left: 516,
      width: 65,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 13,
      canJumpThrough: false,
      
    },

     {
      left: 625,
      width: 63,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 12,
      canJumpThrough: false,
      
    },
     {
      left: 689,
      width: 609,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 9,
      canJumpThrough: false,
      
    },

    {
      left: 812,
      width: 52,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 11,
      canJumpThrough: true,
      
    },

    {
      left: 1332,
      width: 31,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 8,
      canJumpThrough: false,
      
    },
    {
      left: 1387,
      width: 31,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 6,
      canJumpThrough: true,
      
    },
    {
      left: 1448,
      width: 193,
      height: 5,
      opacity: 0.0,
      track: 6,
      canJumpThrough: false,
      
    },

    {
      left: 1056,
      width:174,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 3,
      canJumpThrough: true,
      
    },

    {
      left: 1223,
      width:171,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 2,
      canJumpThrough: false,
      
    },


    {
      left: 1426,
      width:191,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 2,
      canJumpThrough: false,
      
    },
    {
      left: 1617,
      width:383,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 2,
      canJumpThrough: false,
      
    },
//Stage 3
    {
      left: 2000,
      width:369,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 5,
      canJumpThrough: true,
      
    },
    {
      left: 2380,
      width:38,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 1,
      canJumpThrough: false,
      
    },
     {
      left: 2439,
      width:63,
      height: this.PLATFORM_HEIGHT,
      opacity: 0.0,
      track: 5,
      canJumpThrough: false,
      
    },
    
  ];

   this.rubyData = [
      { left: 690,  
         top: this.TRACK_4_BASELINE - this.RUBY_CELLS_HEIGHT },
         { left: 1016,  
         top:335 - this.RUBY_CELLS_HEIGHT },
         { left: 1524,  
         top:this.TRACK_1_BASELINE - this.RUBY_CELLS_HEIGHT },
   ];

   this.baddieData = [
      { platformIndex: 4},
      { platformIndex: 10},
      { platformIndex: 9},
      { platformIndex: 16},
      { platformIndex: 17},
      { platformIndex: 14},
      { platformIndex: 18},
      ];

      this.archerData = [
      {platformIndex: 21}]


this.coinSound = {
    position: 7.1, //seconds
    duration: 588, //milliseconds
    volume: 0.1
  };


  this.explosionSound = {
    position: 4.3, //seconds
    duration: 760, //milliseconds
    volume: 0.1
  };

  this.pianoSound = {
    position: 5.6, //seconds
    duration: 395, //milliseconds
    volume: 0.1
  };

  this.thudSound = {
    position: 3.1, //seconds
    duration: 809, //milliseconds
    volume: 0.3
  };

  this.splashSound = {
    position: 9.3, //seconds
    duration: 1800, //milliseconds
    volume: 0.1
  };

        this.audioChannels = [ //4 channels
    {playing:false, audio: this.audioSprites},
    {playing:false, audio: null},
    {playing:false, audio: null},
    {playing:false, audio: null},
    ];

  this.audioSpriteCountdown = this.audioChannels.length - 1;
 this.graphicsReady = false;

    //Sprite Behaviours
    this.runBehaviour = 
    {
      lastAdvanceTime: 0,

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(sprite.runAnimationRate === 0)
        {
          return;
        }
        if(this.lastAdvanceTime === 0)
        {
          this.lastAdvanceTime = now;
        }
        else if(now - this.lastAdvanceTime > 1000/sprite.runAnimationRate)
        {
          sprite.artist.advance();
          this.lastAdvanceTime = now;
        }
      }
    };

    this.paceBehaviour = {
      setDirection: function(sprite)
      {
        var sRight = sprite.left + sprite.width,
            pRight = sprite.platform.left + sprite.platform.width;

        if(sprite.direction === undefined)
        {
          sprite.direction = jungleRun.RIGHT;
        }
        if(sRight > pRight && sprite.direction === jungleRun.RIGHT)
        {
          sprite.direction = jungleRun.LEFT;
        }
        else if(sprite.left < sprite.platform.left && sprite.direction === jungleRun.LEFT)
        {
          sprite.direction = jungleRun.RIGHT;
        }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime)
      {
        var pixelsToMove = sprite.velocityX * (now - lastAnimationFrameTime)/1000;

        if(sprite.direction === jungleRun.RIGHT)
        {
          sprite.left += pixelsToMove;
        }
        else
        {
          sprite.left -= pixelsToMove;
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        this.setDirection(sprite);
        this.setPosition(sprite, now, lastAnimationFrameTime);
      }
    };

    //archer shoot Behaviour
    this.archerShootBehaviour = 
    {
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var arrow = sprite.arrow,
            MOUTH_OPEN_CELL = 8;

        if(!jungleRun.isSpriteInView(sprite))
        {
          return;
        }
        if(!arrow.visible && sprite.artist.cellIndex === MOUTH_OPEN_CELL)
        {
          arrow.left = sprite.left;
          arrow.visible = true;
        }
      }
    };

    //Move the archer arrow
    this.archerArrowMoveBehaviour = 
    {
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var ARCHER_ARROW_VELOCITY = 250;
        if(sprite.left + sprite.width > sprite.hOffset && sprite.left + sprite.width < sprite.hOffset + sprite.width)
        {
          sprite.visible = false;
        }
        else
        {
          sprite.left -= ARCHER_ARROW_VELOCITY * ((now-lastAnimationFrameTime)/1000);
        }
      }
    };

    //Runner Jump Behaviour
    this.jumpBehaviour = 
    {
      pause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.pause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.pause(now);
        }
      },

      unpause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.unpause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.unpause(now);
        }
      },

      isAscending: function(sprite)
      {
        return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
        var elapsedTime = sprite.ascendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
        return sprite.ascendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishAscent: function(sprite, now)
      {
        sprite.jumpApex = sprite.top;
        sprite.ascendTimer.stop(now);
        sprite.descendTimer.start(now);
      },

      isDescending: function(sprite)
      {
        return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
        var elapsedTime = sprite.descendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.jumpApex + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
        return sprite.descendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishDescent: function(sprite, now)
      {
        sprite.stopJumping();
        sprite.runAnimationRate = jungleRun.RUN_ANIMATION_RATE;
        if(jungleRun.platformUnderneath(sprite))
        {
          sprite.top = sprite.verticalLaunchPosition;
        }
        else
        {
          sprite.fall();
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(!sprite.jumping)
        {
          return;
        }
        if(this.isAscending(sprite))
        {
          jungleRun.isAscendingBool = true;
          if(!this.isDoneAscending(sprite, now))
          {
            this.ascend(sprite, now);
          }
          else
          {
            jungleRun.isAscendingBool = false;
            this.finishAscent(sprite, now);
          }
        }
        else if(this.isDescending(sprite))
        {
          if(!this.isDoneDescending(sprite, now))
          {
            this.descend(sprite, now);
          }
          else 
          {
            this.finishDescent(sprite, now);
          }
        }
      }
    };


    this.collideBehaviour={
      //Three step process
      //1. What should we consider for collision?
      //2. Is there a collision
      //3. React to the collision
      isCandidateForCollision: function(sprite, otherSprite)
      {
        var s, o;
        s = sprite.calculateCollisionRectangle(),
        o = otherSprite.calculateCollisionRectangle();

        candidate =  o.left < s.right && sprite !== otherSprite &&
        sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
        return candidate;
      },

      didCollide: function(sprite, otherSprite, context)
      {
        var o = otherSprite.calculateCollisionRectangle(),
            r = sprite.calculateCollisionRectangle();
        //Determine if any of the four corners of the runner's
        //sprite or the centre lie within the other sprites
        //bounding box
        context.beginPath();
        context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);
        var collision = context.isPointInPath(r.left, r.top) || context.isPointInPath(r.right, r.top) || context.isPointInPath(r.left, r.bottom) || context.isPointInPath(r.right, r.bottom) || context.isPointInPath(r.centreX, r.centreY);
        return collision;

      },

      processPlatformCollisionDuringJump: function(sprite, platform)
      {
        
        var isDescending = sprite.descendTimer.isRunning();
        sprite.stopJumping();
        if(isDescending)
        {
          sprite.track = platform.track;
          sprite.top = jungleRun.calculatePlatformTop(sprite.track) - sprite.height;
          jungleRun.playSound(jungleRun.thudSound);
        }
        else
        {
          sprite.fall();
          
        }
      },

      processBadGuyCollision: function(sprite)
      {
        jungleRun.playSound(jungleRun.explosionSound);
        console.log("COLLIDE!!");
        jungleRun.explode(sprite);
        jungleRun.shake();
        jungleRun.loseLife();
         
      },

      processCollision: function(sprite, otherSprite)
      {
        //console.log(sprite.type, otherSprite.type);
        if(sprite.jumping && 'platform' === otherSprite.type)
        {
            if(otherSprite.canJumpThrough && jungleRun.isAscendingBool)
            {

            }
            else
            {
                this.processPlatformCollisionDuringJump(sprite, otherSprite);
            }
        }
        else if('coin' === otherSprite.type || 'ruby' === otherSprite.type)
        {
          this.processAssetCollision(otherSprite);
                             
        }       
        else if('bee' === otherSprite.type || 'archer arrow' === otherSprite.type || 'baddie' === otherSprite.type || 'archer' === otherSprite.type)
        {
          this.processBadGuyCollision(sprite);
        }
        else if('button' === otherSprite.type)
        {
          if(sprite.jumping && sprite.descendTimer.isRunning() || sprite.falling)
          {
            otherSprite.detonating = true;
          }
        }
      },

      processAssetCollision: function (sprite) {
         sprite.visible = false; // sprite is the asset

         if (sprite.type === 'coin') {
          jungleRun.score += 100; 
             jungleRun.playSound(jungleRun.coinSound);
         }
          else if(sprite.type === 'ruby')
        {
          jungleRun.score += 250; 
          jungleRun.playSound(jungleRun.pianoSound);
        }
      },


      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var otherSprite;

          if ( ! jungleRun.playing) {
            return;
         }
        for(var i=0; i < jungleRun.sprites.length; ++i)
        {
          otherSprite = jungleRun.sprites[i];
          if(this.isCandidateForCollision(sprite, otherSprite))
          {
            if(this.didCollide(sprite, otherSprite, context))
            {
                this.processCollision(sprite, otherSprite);
              
            }
          }
        }
      }
    };

    //Fall Behaviour
    this.fallBehaviour = {

      pause: function(sprite, now)
      {
        sprite.fallTimer.pause(now);
      },

      unpause: function(sprite, now)
      {
        sprite.fallTimer.unpause(now);
      },

      isOutOfPlay: function(sprite)
      {
        return sprite.top > jungleRun.canvas.height;
        
      },

      setSpriteVelocity: function(sprite, now)
      {
        sprite.velocityY = sprite.initialVelocityY + jungleRun.GRAVITY_FORCE*(sprite.fallTimer.getElapsedTime(now)/1000)*jungleRun.PIXELS_PER_METRE;
        console.log(sprite.velocityY);
      },

      calculateVerticalDrop: function(sprite, now, lastAnimationFrameTime)
      {
        return sprite.velocityY * (now-lastAnimationFrameTime)/1000;
      },

      willFallBelowCurrentTrack: function(sprite, dropDistance)
      {
        return sprite.top+sprite.height+dropDistance > jungleRun.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function(sprite)
      {
        sprite.stopFalling();
        jungleRun.putSpriteOnTrack(sprite, sprite.track);
        jungleRun.playSound(jungleRun.thudSound);
      },

      moveDown: function(sprite, now, lastAnimationFrameTime)
      {
        var dropDistance;
        this.setSpriteVelocity(sprite, now);
        dropDistance = this.calculateVerticalDrop(sprite, now, lastAnimationFrameTime);
        console.log(dropDistance);
        if(!this.willFallBelowCurrentTrack(sprite, dropDistance))
        {
          sprite.top += dropDistance;
        }
        else
        {
          if(jungleRun.platformUnderneath(sprite))
          {
            this.fallOnPlatform(sprite);
            sprite.stopFalling();
          }
          else
          {
            sprite.track--;
            sprite.top += dropDistance;
          }
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(!sprite.falling)
        {
          if(!sprite.jumping && !jungleRun.platformUnderneath(sprite))
          {
            sprite.fall();
          }
        }
        else
        {
          if(this.isOutOfPlay(sprite) || sprite.exploding)
          {
            sprite.stopFalling();
            if(this.isOutOfPlay(sprite))
            {              
              jungleRun.loseLife();
              jungleRun.playSound(jungleRun.splashSound);
              jungleRun.runner.visible = false;
            }
          }
          else
          {
            this.moveDown(sprite, now, lastAnimationFrameTime);
          }
        }
      }

    };

    //Runner explosions

    this.runnerExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells, this.RUNNER_EXPLOSION_DURATION, function (sprite, now, fps){return sprite.exploding;}, function(sprite, animator){ sprite.exploding = false;});

    //Detonate buttons
        this.blueButtonDetonateBehaviour = {
      execute: function(sprite, now, fps, lastAnimationFrameTime)
      {
        var BUTTON_REBOUND_DELAY = 10000,
            SECOND_DELAY = 700;
        if(!sprite.detonating)
        {
          return;
        }
        sprite.artist.cellIndex = 1;
        jungleRun.explode(jungleRun.archers[0]);
        setTimeout(function(){   
          jungleRun.setTimeRate(0.1);
        }, SECOND_DELAY);
        sprite.detonating = false;
        setTimeout(function(){
          sprite.artist.cellIndex = 0;
        }, BUTTON_REBOUND_DELAY);        
        jungleRun.bgVelocity = 0;
        jungleRun.fadeInElements(jungleRun.gameCompleteElement);
        jungleRun.gameFinish = true;
        jungleRun.gamePaused = false;
      }
    };

  //Sprites
  //This costs more memory but is faster to iterate through all of the sprites of a particular type
  this.sprites = [];
  this.bees = [];
  this.buttons = [];
  this.coins = [];
  this.platforms = [];
  this.rubies = [];
  this.baddies = [];
  this.archers = [];

  this.platformArtist = 
  {
    draw: function(sprite, context)
    {
      var PLATFORM_STROKE_WIDTH = 1.0,
        PLATFORM_STROKE_STYLE = 'black',
        top;
      top = jungleRun.calculatePlatformTop(sprite.track);
      context.lineWidth = PLATFORM_STROKE_WIDTH;
      context.strokeStyle = PLATFORM_STROKE_STYLE;
      context.fillStyle = sprite.fillStyle;

      context.strokeRect(sprite.left, top, sprite.width,
      sprite.height);
      context.fillRect(sprite.left, top, sprite.width,
      sprite.height);

    }
  }
}; //End of constructor

jungleRun.prototype = 
{
  createSprites: function()
  {
    this.createPlatformSprites();
    this.createBeeSprites();
    this.createButtonSprites();
    this.createCoinSprites();
    this.createRunnerSprite ();
    this.createRubySprites();
    this.createBaddieSprites();
                this.createArcherSprites();
    this.initializeSprites();
    //Add all of the sprites to a single array
    this.addSpritesToSpriteArray();
  },


    explode: function (sprite) {
      if ( ! sprite.exploding) {
        if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }
         sprite.exploding = true;
         
      }
   },

  addSpritesToSpriteArray: function()
  {
    for(var i =0; i < this.platforms.length; ++i)
    {
      this.sprites.push(this.platforms[i]);
    }
    for(var i =0; i < this.bees.length; ++i)
    {
      this.sprites.push(this.bees[i]);
    }
    for(var i =0; i < this.buttons.length; ++i)
    {
      this.sprites.push(this.buttons[i]);
    }
    for(var i =0; i < this.coins.length; ++i)
    {
      this.sprites.push(this.coins[i]);
    }
    for(var i =0; i < this.rubies.length; ++i)
    {
      this.sprites.push(this.rubies[i]);
    }
    for(var i =0; i < this.baddies.length; ++i)
    {
      this.sprites.push(this.baddies[i]);
    }
for(var i =0; i < this.archers.length; ++i)
    {
      this.sprites.push(this.archers[i]);
    }


    this.sprites.push(this.runner);
  },

  initializeSprites: function()
  {
    this.positionSprites(this.bees, this.beeData);
    this.positionSprites(this.buttons, this.buttonData);
    this.positionSprites(this.coins, this.coinData);
    this.positionSprites(this.rubies, this.rubyData);
    this.positionSprites(this.baddies, this.baddieData);
    this.positionSprites(this.archers, this.archerData);

    this.equipRunner();
    this.armArchers();
  },

  equipRunner: function()
  {
    this.equipRunnerForJumping();
    this.equipRunnerForFalling();
    this.runner.direction = jungleRun.LEFT;
  },

  equipRunnerForJumping: function()
  {
    var INITIAL_TRACK = 4,
        RUNNER_JUMP_HEIGHT = 100,
        RUNNER_JUMP_DURATION = 800,
        EASING_FACTOR = 1.1;

    this.runner.JUMP_HEIGHT = RUNNER_JUMP_HEIGHT;
    this.runner.JUMP_DURATION = RUNNER_JUMP_DURATION;
    this.runner.jumping = false;
    this.runner.track = INITIAL_TRACK;
    this.runner.ascendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseOutEasingFunction(EASING_FACTOR));
    this.runner.descendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseInEasingFunction(EASING_FACTOR));

    this.runner.jump = function()
    {
      if(this.jumping)
      {
        return;
      }
      this.jumping = true;
      this.runAnimationRate = 0;
      this.verticalLaunchPosition = this.top;
      this.ascendTimer.start(jungleRun.timeSystem.calculateGameTime());
    };

    this.runner.stopJumping = function()
    {
      this.descendTimer.stop();
      this.jumping = false;
    }; 
  },

  equipRunnerForFalling: function()
  {
    this.runner.fallTimer = new AnimationTimer();
    this.runner.fall = function(initialVelocity)
    {
      this.falling = true;
      this.velocityY = initialVelocity || 0;
      this.initialVelocityY = initialVelocity || 0;
      this.fallTimer.start(jungleRun.timeSystem.calculateGameTime());
    };
    this.runner.stopFalling = function()
    {
      this.falling = false;
      this.velocityY = 0;
      this.fallTimer.stop(jungleRun.timeSystem.calculateGameTime());
    };
  },

  armArchers: function()
  {
    var archer,
        archerArrowArtist = new SpriteSheetArtist(this.spritesheet, this.archerArrowCells);

    for(var i=0; i<this.archers.length; ++i)
    {
      archer = this.archers[i];
      archer.arrow = new Sprite('archer arrow', archerArrowArtist, [this.archerArrowMoveBehaviour]);
      archer.arrow.width = jungleRun.ARCHER_ARROW_CELLS_WIDTH;
      archer.arrow.height = jungleRun.ARCHER_ARROW_CELLS_HEIGHT;
      archer.arrow.top = archer.top + archer.arrow.height/2;
      archer.arrow.left = archer.left + archer.arrow.width/2;
      archer.arrow.visible = false;
      //archer arrows maintain reference to their archer
      archer.arrow.archer = archer;
      this.sprites.push(archer.arrow);
    }
  },

  positionSprites: function(sprites, spriteData)
  {
    var sprite;
    for(var i=0; i<sprites.length; ++i)
    {
      sprite = sprites[i];
      if(spriteData[i].platformIndex)
      {
        this.putSpriteOnPlatform(sprite, this.platforms[spriteData[i].platformIndex]);
      }
      else
      {
        sprite.top = spriteData[i].top;
        sprite.left = spriteData[i].left;
      }
    }
  },

  putSpriteOnPlatform: function(sprite, platformSprite)
  {
    sprite.top = platformSprite.top - sprite.height;
    sprite.left = platformSprite.left;
    sprite.platform = platformSprite;
  },

  putSpriteOnTrack: function(sprite, track)
  {
    sprite.track = track;
    sprite.top = this.calculatePlatformTop(sprite.track) - sprite.height;
  },

  platformUnderneath: function(sprite, track)
  {
    var platform, platformUnderneath,
        sr = sprite.calculateCollisionRectangle(),
        pr;
    if(track === undefined)
    {
      track = sprite.track;
    }
    for(var i=0; i<jungleRun.platforms.length; ++i)
    {
      platform = jungleRun.platforms[i];
      pr = platform.calculateCollisionRectangle();
      if(track === platform.track)
      {
        if(sr.right > pr.left && sr.left < pr.right)
        {
          platformUnderneath = platform;
          break;
        }
      }
    }
    return platformUnderneath;
  },

  createBeeSprites: function()
  {
    var bee,
        BEE_FLAP_DURATION = 80,
        BEE_FLAP_INTERVAL = 100;
    for(var i=0; i<this.beeData.length;++i)
    {
      bee = new Sprite('bee', new SpriteSheetArtist(this.spritesheet, this.beeCells), [new CycleBehaviour(BEE_FLAP_DURATION, BEE_FLAP_INTERVAL), new CellSwitchBehaviour(this.explosionCells, this.BAD_GUYS_EXPLOSION_DURATION, function(sprite, now, fps){return sprite.exploding;}, function(sprite, animator){sprite.exploding = false;})]);
      bee.width = this.BEE_CELLS_WIDTH;
      bee.height = this.BEE_CELLS_HEIGHT;
      this.bees.push(bee);
    }
  },

  createButtonSprites: function()
  {
    var button;
    for(var i=0; i<this.buttonData.length;++i)
    {
        button = new Sprite('button', new SpriteSheetArtist(this.spritesheet, this.blueButtonCells));
        button.behaviours = [this.paceBehaviour, this.blueButtonDetonateBehaviour];

      button.width = this.BUTTON_CELLS_WIDTH;
      button.height = this.BUTTON_CELLS_HEIGHT;
      button.velocityX = this.BUTTON_PACE_VELOCITY;
      this.buttons.push(button);
    }
  },

  createCoinSprites: function()
  {
    var coin,
        COIN_SPARKLE_DURATION = 300,
        COIN_SPARKLE_INTERVAL = 600;
        
    for(var i=0; i<this.coinData.length;++i)
    {
      if(i%2 === 0)
      {
        coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.goldCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL)]);
      }
      else
      {
        coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.blueCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL)]);
      }
      coin.width = this.COIN_CELLS_WIDTH;
      coin.height = this.COIN_CELLS_HEIGHT;
      coin.value = 50;
      this.coins.push(coin);
    }
  },

  createPlatformSprites: function()
  {
    var sprite, pd,
        PULSE_DURATION = 800,
        PULSE_OPACITY_THRESHOLD = 0.1;
    for(var i=0; i<this.platformData.length;++i)
    {
      pd = this.platformData[i];
      sprite = new Sprite('platform', this.platformArtist);
      sprite.left = pd.left;
      sprite.width = pd.width;
      sprite.height = pd.height;
      sprite.opacity = pd.opacity;
      sprite.track = pd.track;
      sprite.button = pd.button;
      sprite.canJumpThrough = pd.canJumpThrough;

      sprite.top = this.calculatePlatformTop(pd.track);

      this.platforms.push(sprite);
    }
  },

  createRubySprites: function()
  {
    var ruby,
        RUBY_SPARKLE_DURATION = 100,
        RUBY_SPARKLE_INTERVAL = 500;

    for(var i=0; i<this.rubyData.length;++i)
    {
      ruby = new Sprite('ruby', new SpriteSheetArtist(this.spritesheet, this.rubyCells), [new CycleBehaviour(RUBY_SPARKLE_DURATION, RUBY_SPARKLE_INTERVAL)]);
      ruby.width = this.RUBY_CELLS_WIDTH;
      ruby.height = this.RUBY_CELLS_HEIGHT;
      ruby.value = 200;
      this.rubies.push(ruby);
    }
  },

  createRunnerSprite: function()
  {
    var RUNNER_LEFT = 50,
        RUNNER_HEIGHT = 53,
        STARTING_RUN_ANIMATION_RATE = 0,
        STARTING_RUNNER_TRACK = 4;

    this.runner = new Sprite('runner', new SpriteSheetArtist(this.spritesheet, this.runnerCellsRight), [this.runBehaviour, this.jumpBehaviour, this.collideBehaviour, this.fallBehaviour, this.runnerExplodeBehaviour]);
    this.runner.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
    this.runner.track = STARTING_RUNNER_TRACK;
    this.runner.left = RUNNER_LEFT;
    this.runner.top = this.calculatePlatformTop(this.runner.track) - RUNNER_HEIGHT;
    this.runner.width = this.RUNNER_CELLS_WIDTH;
    this.runner.height = this.RUNNER_CELLS_HEIGHT;
    this.sprites.push(this.runner);
  },

  createBaddieSprites: function()
  {
    var baddie,
        BADDIE_CYCLE_DURATION = 300,
        BADDIE_CYCLE_INTERVAL = 3500,
        BOUNCE_DURATION_BASE = 700,
        BOUNCE_HEIGHT_BASE = 60;
    for(var i=0; i<this.baddieData.length;++i)
    {
            if(i%2 === 0)
      {
      baddie = new Sprite('baddie', new SpriteSheetArtist(this.spritesheet, this.baddieCells), [this.paceBehaviour, new CycleBehaviour(BADDIE_CYCLE_DURATION, BADDIE_CYCLE_INTERVAL)]);
    }
    else
    {
      baddie = new Sprite('baddie', new SpriteSheetArtist(this.spritesheet, this.baddieCells3), [this.paceBehaviour, new CycleBehaviour(BADDIE_CYCLE_DURATION, BADDIE_CYCLE_INTERVAL), new BounceBehaviour(BOUNCE_DURATION_BASE, BOUNCE_HEIGHT_BASE)]);
    }
      baddie.width = this.BADDIE_CELLS_WIDTH;
      baddie.height = this.BADDIE_CELLS_HEIGHT;
      baddie.velocityX = jungleRun.BADDIE_PACE_VELOCITY;
      this.baddies.push(baddie);
    }
  },

  createArcherSprites: function()
  {
    var archer,
        ARCHER_CYCLE_DURATION = 300,
        ARCHER_CYCLE_INTERVAL = 3500,
        BOUNCE_DURATION_BASE = 800,
        BOUNCE_HEIGHT_BASE = 50;
    for(var i=0; i<this.archerData.length;++i)
    {
      archer = new Sprite('archer', new SpriteSheetArtist(this.spritesheet, this.archerCells), [this.archerShootBehaviour, new CycleBehaviour(ARCHER_CYCLE_DURATION, ARCHER_CYCLE_INTERVAL), new CellSwitchBehaviour(this.explosionCells, this.BAD_GUYS_EXPLOSION_DURATION, function(sprite, now, fps){return sprite.exploding;}, function(sprite, animator){sprite.exploding = false;})]);
  
      archer.width = this.ARCHER_CELLS_WIDTH;
      archer.height = this.ARCHER_CELLS_HEIGHT;
      this.archers.push(archer);
    }
  },


  //Animation
  animate: function(now)
  {
    //Replace the time passed to animate by the browser
    //with our own game time
    now = jungleRun.timeSystem.calculateGameTime();
    if(jungleRun.paused)
    {
      setTimeout(function(){
        requestAnimationFrame(jungleRun.animate);
      }, jungleRun.PAUSED_CHECK_INTERVAL);
    }
    else
    {

      fps = jungleRun.calculateFps(now);

      
      jungleRun.draw(now);
      jungleRun.lastAnimationFrameTime = now;
      requestAnimationFrame(jungleRun.animate);
    }
  },

  calculateFps: function(now)
  {
    var fps = 1/(now - jungleRun.lastAnimationFrameTime) * 1000*
    this.timeRate;
    if(now - jungleRun.lastFpsUpdateTime > 1000)
    {
      jungleRun.lastFpsUpdateTime = now;
      jungleRun.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
      jungleRun.scoreElement.innerHTML = jungleRun.score; 
      if(fps < 30)
      {
        this.fadeInElements(this.lowFramesElement);
        this.fadeOutElements(this.lowFramesElement, 3000);
      }
    }
    return fps;
  },


  initializeImages: function()
  {
    jungleRun.platformImages.src = 'images/platforms.png';
    jungleRun.background.src = 'images/clouds.png';
    jungleRun.loadingAnimatedGIFElement.src = 'images/character.png';
    this.spritesheet.src = 'images/spritesheet.png';

    this.background.onload = function(e)
    {
      jungleRun.backgroundLoaded();
    };

    this.loadingAnimatedGIFElement.onload = function(e)
    {
      jungleRun.loadingAnimationLoaded();
    };
  },

  backgroundLoaded: function()
  {
    var LOADING_SCREEN_TRANSITION_DURATION = 2000;
    this.fadeOutElements(this.loadingElement, LOADING_SCREEN_TRANSITION_DURATION);
    setTimeout(function(e){
      jungleRun.startGame();
      jungleRun.gameStarted = true;
    }, LOADING_SCREEN_TRANSITION_DURATION);
  },

  loadingAnimationLoaded: function()
  {
    if(!this.gameStarted)
    {
      this.fadeInElements(this.loadingAnimatedGIFElement, this.loadingTitleElement);
    }
  },


    spritesheetLoaded: function () {
      var LOADING_SCREEN_TRANSITION_DURATION = 2000;

      this.graphicsReady = true;

      this.fadeOutElements(this.loadingElement, 
         LOADING_SCREEN_TRANSITION_DURATION);

      setTimeout ( function () {
         if (! jungleRun.gameStarted) {
            jungleRun.startGame();
         }
      }, LOADING_SCREEN_TRANSITION_DURATION);
   },


  startGame: function()
  {
    //this.togglePaused();
    this.revealGame();
    if(jungleRun.mobile)
    {
      this.fadeInElements(jungleRun.mobileWelcomeToast);
    }
    else
    {
     this.revealInitialToast();
     this.playing = true;
    }
    this.startMusic();
    this.timeSystem.start();
    this.setTimeRate(this.timeRate);
    this.gameStarted = true;
    this.highScore = localStorage.getItem("highScore");
    if( this.highScore == 'undefined' ) 
    {
      this.highScore = 0;
    }
    
    window.requestAnimationFrame(jungleRun.animate);
  },

  setTimeRate: function(rate)
  {
    this.timeRate = rate;
    this.timeSystem.setTransducer(function(now)
    {
      return now * jungleRun.timeRate;
    });
  },

  revealGame: function()
  {
    var DIM_CONTROLS_DELAY = 5000;
    this.revealTopChromeDimmed();
    this.revealCanvas();
    this.revealBottomChrome();

    setTimeout(function() {
      jungleRun.dimControls();
      jungleRun.revealTopChrome();
    }, DIM_CONTROLS_DELAY);
  },

  revealTopChromeDimmed: function()
  {
    var DIM = 0.25;
    this.scoreElement.style.display = 'block';
    this.fpsElement.style.display = 'block';
    this.livesElement.style.display = 'block';
    setTimeout(function(){
      jungleRun.scoreElement.style.opacity = 'block';
      jungleRun.fpsElement.style.opacity = 'block';
      jungleRun.livesElement.style.opacity = 'block';
    }, this.SHORT_DELAY);
  },

  revealCanvas: function()
  {
    this.fadeInElements(this.canvas);
  },

  revealBottomChrome: function()
  {
    this.fadeInElements(this.soundAndMusicElement, this.instructionElement, this.copyrightElement);
  },

  dimControls: function()
  {
    var FINAL_OPACITY = 0.5;
    jungleRun.instructionElement.style.opacity = FINAL_OPACITY;
    jungleRun.soundAndMusicElement.style.opacity = FINAL_OPACITY;
  },

  revealTopChrome: function()
  {
    jungleRun.highScoreElement.innerHTML = 'High Score: ' + jungleRun.highScore;
    this.fadeInElements(this.fpsElement, this.scoreElement, this.livesElement, this.highScoreElement, this.gameLevelElement);
  },

  revealInitialToast: function()
  {
    var INITIAL_TOAST_DELAY = 1500,
        INITIAL_TOAST_DURATION = 3000;
    setTimeout(function(){
      jungleRun.revealToast('Pick up valuables, avoid enemies and reach the end of the level.', INITIAL_TOAST_DURATION);
    }, INITIAL_TOAST_DELAY);
  },



  draw: function(now)
  {
    jungleRun.setPlatformVelocity();
    jungleRun.setOffsets(now);
    jungleRun.drawBackground();
    this.updateSprites(now);
    this.drawScore(this.score);
    this.levelUp(this.score, this.gameLevel);
    this.drawLevel(this.gameLevel);
    this.drawSprites();
    if (this.mobileInstructionsVisible) 
    {
         jungleRun.drawMobileInstructions();
    }
  },

  drawScore: function(score)
  {
    jungleRun.scoreElement.innerHTML = score + ' score';
  },

    levelUp: function(score, gameLevel)
  {
    if(score >= 200 && gameLevel === 1) //level 2
    {
      this.timeRate += 0.5; //make the game faster
      this.gameLevel += 1; //increment the level 
    }
    if(score >= 500 && gameLevel === 2)
    {
      this.timeRate += 0.5; //make the game faster
      this.gameLevel += 1; //increment the level 
    }
    if(score >= 1000 && gameLevel === 3)
    {
      this.timeRate += 0.5; //make the game faster
      this.gameLevel += 1; //increment the level 
    }
    if(score >= 1500 && gameLevel === 4)
    {
      this.timeRate += 0.5; //make the game faster
      this.gameLevel += 1; //increment the level 
    }
    if(score >= 2000 && gameLevel === 5)
    {
      this.timeRate += 0.5; //make the game faster
      this.gameLevel += 1; //increment the level 
    }
  },

  drawLevel: function(gameLevel)
  {
    jungleRun.gameLevelElement.innerHTML = 'Level: ' + gameLevel;
  },

  setHighScore: function(totalScore, highScore)
  {
    //console.log(totalScore);
    //console.log('entering set high score');
    //console.log(highScore);
    if(highScore <= totalScore)
    {
      //console.log('setting high score');
      highScore = totalScore;
      //console.log(highScore);
    }
    jungleRun.highScoreElement.innerHTML = 'High Score: ' + highScore;
    // Every time the player dies, the new highScore is set
    localStorage.setItem("highScore", highScore);
    return highScore;
  },

  updateSprites: function(now)
  {
    var sprite;
    for(var i=0; i<this.sprites.length; i++)
    {
      sprite = this.sprites[i];
      if(sprite.visible && this.isSpriteInView(sprite))
      {
        //this.context.translate(-sprite.hOffset, 0);
        sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
        //this.context.translate(sprite.hOffset, 0);
      }
    }
  },

  drawSprites: function()
  {
    var sprite;
    for(var i=0; i<this.sprites.length; i++)
    {
      //console.log(this.sprite[i].type);
      sprite = this.sprites[i];
      if(sprite.visible && this.isSpriteInView(sprite))
      {
        this.context.translate(-sprite.hOffset, 0);
        sprite.draw(this.context);
        this.context.translate(sprite.hOffset, 0);
      }
    }
   
 
  },

  isSpriteInView: function(sprite)
  {
    return sprite.left+sprite.width > sprite.hOffset && sprite.left < sprite.hOffset + this.canvas.width;
  },

  drawBackground: function()
  {
    jungleRun.context.translate(-jungleRun.backgroundOffset, 0);
    jungleRun.context.drawImage(jungleRun.background, 0, 0);
    jungleRun.context.drawImage(jungleRun.background, jungleRun.background.width, 0);
    jungleRun.context.translate(jungleRun.backgroundOffset, 0);

    jungleRun.context.translate(-jungleRun.backgroundOffset *jungleRun.PLATFORM_VELOCITY_MULTIPLER, 0);
    jungleRun.context.drawImage(jungleRun.platformImages, 0, 0);
    jungleRun.context.drawImage(jungleRun.platformImages, jungleRun.platformImages.width, 0);
    jungleRun.context.translate(jungleRun.backgroundOffset*jungleRun.PLATFORM_VELOCITY_MULTIPLER, 0);
    
  },

  drawRunner: function()
  {
    jungleRun.context.drawImage(jungleRun.runner, jungleRun.RUNNER_LEFT, jungleRun.calculatePlatformTop(jungleRun.STARTING_RUNNER_TRACK) - jungleRun.runner.height);
  },

  drawPlatform: function(data)
  {
    var platformTop = jungleRun.
    calculatePlatformTop(data.track);
    jungleRun.context.lineWidth = jungleRun.PLATFORM_STROKE_WIDTH;
    jungleRun.context.strokeStyle = jungleRun.PLATFORM_STROKE_STYLE;
    jungleRun.context.fillStyle = data.fillStyle;
    jungleRun.context.globalAlpha = data.opacity;
    jungleRun.context.strokeRect(data.left, platformTop, data.width, data.height);
    jungleRun.context.fillRect(data.left, platformTop, data.width, data.height);
  },

  drawPlatforms: function()
  {
    var index;
    jungleRun.context.translate(-jungleRun.platformOffset, 0);
    for(index = 0; index < jungleRun.platformData.length; ++index)
    {
      jungleRun.drawPlatform(jungleRun.platformData[index]);
    }
    
    jungleRun.context.translate(jungleRun.platformOffset, 0);
  },

  calculatePlatformTop: function(track)
  {
    if(track === 1)
    {
      return jungleRun.TRACK_1_BASELINE;
    }
    else if(track === 2)
    {
      return jungleRun.TRACK_2_BASELINE;
    }
    else if(track === 3)
    {
      return jungleRun.TRACK_3_BASELINE;
    }
    else if(track === 4)
    {
      return jungleRun.TRACK_4_BASELINE;
    }
     else if(track === 5)
    {
      return jungleRun.TRACK_5_BASELINE;
    }
     else if(track === 6)
    {
      return jungleRun.TRACK_6_BASELINE;
    }
     else if(track === 7)
    {
      return jungleRun.TRACK_7_BASELINE;
    }
    else if(track === 8)
    {
      return jungleRun.TRACK_8_BASELINE;
    }
    else if(track === 9)
    {
      return jungleRun.TRACK_9_BASELINE;
    }
     else if(track === 10)
    {
      return jungleRun.TRACK_10_BASELINE;
    }
     else if(track === 11)
    {
      return jungleRun.TRACK_11_BASELINE;
    }
     else if(track === 12)
    {
      return jungleRun.TRACK_12_BASELINE;
    }
    else if(track === 13)
    {
      return jungleRun.TRACK_13_BASELINE;
    }
  },

  setBackgroundOffset: function(now)
  {
    jungleRun.backgroundOffset += jungleRun.bgVelocity*(now - jungleRun.lastAnimationFrameTime)/1000;
    if(jungleRun.backgroundOffset < 0 || jungleRun.backgroundOffset > jungleRun.background.width)
    {
      jungleRun.backgroundOffset = 0;
    }
  },

  turnLeft: function()
  {
    jungleRun.bgVelocity = -jungleRun.BACKGROUND_VELOCITY;
    this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.runner.artist.cells = this.runnerCellsLeft;
    this.runner.direction = jungleRun.LEFT;
  },

  turnRight: function()
  {
    jungleRun.bgVelocity = jungleRun.BACKGROUND_VELOCITY;
    this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.runner.artist.cells = this.runnerCellsRight;
    this.runner.direction = jungleRun.RIGHT;
  },

  setPlatformVelocity: function()
  {
    jungleRun.platformVelocity = jungleRun.bgVelocity * jungleRun.PLATFORM_VELOCITY_MULTIPLER;
  },

setPlatformOffset: function(now)
  {
    jungleRun.platformOffset += jungleRun.platformVelocity*(now - jungleRun.lastAnimationFrameTime)/1000;
    jungleRun.platformBackgroundOffset += jungleRun.platformVelocity*(now - jungleRun.lastAnimationFrameTime)/1000;
    if(jungleRun.platformOffset > 2*jungleRun.platformImages.width)
    {
      jungleRun.turnLeft();
    }
    else if(jungleRun.platformOffset < 0)
    {
      jungleRun.turnRight();
    }
  },
  setOffsets: function(now)
  {
    jungleRun.setBackgroundOffset(now);
    jungleRun.setSpriteOffsets(now);
    jungleRun.setPlatformOffset(now);
  },

  setSpriteOffsets: function (now) {
      var sprite;
   
      // In step with platforms
      this.spriteOffset +=
         this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if ('runner' !== sprite.type) {
            sprite.hOffset = this.spriteOffset; 
         }
      }
   },

 togglePaused: function()
  {
    var now = this.timeSystem.calculateGameTime();
    this.paused = !this.paused;
    if(this.paused)
    {
      this.pauseStartTime = now;
      this.fadeInElements(this.pausedElement);
    }
    else
    {
      this.lastAnimationFrameTime += (now - this.pauseStartTime);
      var PAUSED_TRANSITION_DURATION = 150;
      this.fadeOutElements(this.pausedElement, PAUSED_TRANSITION_DURATION);
      this.revealToast("Welcome Back", 2000);
    }
  
 if(this.musicOn)
   {
     if(this.paused)
     {
       this.musicElement.pause();
     }
     else      {
        this.musicElement.play();
      }
    }
  },
 
  togglePausedStateOfAllBehaviours: function (now) {
      var behaviour;
   

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviours.length; ++j) {
            behaviour = sprite.behaviours[j];

            if (this.paused) {
               if (behaviour.pause) {
                  behaviour.pause(sprite, now);
               }
            }
            else {
               if (behaviour.unpause) {
                  behaviour.unpause(sprite, now);
               }
            }
         }
      }
   },


  revealToast: function(text, duration)
  {
    var DEFAULT_TOAST_DURATION = 1000;
    duration = duration || DEFAULT_TOAST_DURATION;
    this.startToastTransition(text, duration);
    setTimeout(function(e){
      jungleRun.hideToast();
    }, duration);
  },

  startToastTransition: function(text, duration)
  {
    this.toastElement.innerHTML = text;
    this.fadeInElements(this.toastElement);
  },

  hideToast: function()
  {
    var TOAST_TRANSITION_DURATION = 500;
    this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
  },

  fadeInElements: function()
  {
    var args = arguments;
    for(var i=0; i < args.length; ++i)
    {
      args[i].style.display = 'block';
    }
    setTimeout(function(e){
      for(var i=0; i < args.length; ++i)
      {
        args[i].style.opacity = jungleRun.OPAQUE;
      }
    }, this.SHORT_DELAY);
  },

  fadeOutElements: function()
  {
    var args = arguments,
        fadeDuration = args[args.length-1];

    for(var i=0; i < args.length-1; ++i)
    {
      args[i].style.opacity = jungleRun.TRANSPARENT;
    }
    setTimeout(function(e){
      for(var i=0; i < args.length-1; ++i)
      {
        args[i].style.display = 'none';
      }
    }, fadeDuration);
  },

  //Effects
  explode: function(sprite)
  {
    if(!sprite.exploding)
    {
      if(sprite.runAnimationRate === 0)
      {
        sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
      }
      sprite.exploding = true;
    }
  },

  //added 07/04/2017
  shake: function () {
      var SHAKE_INTERVAL = 80, // milliseconds
          v = jungleRun.BACKGROUND_VELOCITY*1.5,
          ov = jungleRun.backgroundVelocity; // ov means original velocity
   
      jungleRun.backgroundVelocity = -v;

      setTimeout( function (e) {
       jungleRun.backgroundVelocity = v;
       setTimeout( function (e) {
          jungleRun.backgroundVelocity = -v;
          setTimeout( function (e) {
             jungleRun.backgroundVelocity = v;
             setTimeout( function (e) {
                jungleRun.backgroundVelocity = -v;
                setTimeout( function (e) {
                   jungleRun.backgroundVelocity = v;
                   setTimeout( function (e) {
                      jungleRun.backgroundVelocity = -v;
                      setTimeout( function (e) {
                         jungleRun.backgroundVelocity = v;
                         setTimeout( function (e) {
                            jungleRun.backgroundVelocity = -v;
                            setTimeout( function (e) {
                               jungleRun.backgroundVelocity = v;
                               setTimeout( function (e) {
                                  jungleRun.backgroundVelocity = -v;
                                  setTimeout( function (e) {
                                     jungleRun.backgroundVelocity = v;
                                     setTimeout( function (e) {
                                        jungleRun.backgroundVelocity = ov;
                                     }, SHAKE_INTERVAL);
                                  }, SHAKE_INTERVAL);
                               }, SHAKE_INTERVAL);
                            }, SHAKE_INTERVAL);
                         }, SHAKE_INTERVAL);
                      }, SHAKE_INTERVAL);
                   }, SHAKE_INTERVAL);
                }, SHAKE_INTERVAL);
             }, SHAKE_INTERVAL);
          }, SHAKE_INTERVAL);
       }, SHAKE_INTERVAL);
     }, SHAKE_INTERVAL);
   },

   loseLife: function()
   {
    var TRANSITION_DURATION = 3000;
    this.lives--;
    this.highScore = this.setHighScore(this.score, this.highScore);
    this.score = 0;
    this.gameLevel = 1;
    this.startLifeTransition(jungleRun.RUNNER_EXPOSION_DURATION);
    setTimeout(function(){
      jungleRun.endLifeTransition();
    }, TRANSITION_DURATION);
   },

   win: function()
   {
    var TRANSITION_DURATION = 3000;
    this.highScore = this.setHighScore(this.score, this.highScore);
    this.gameLevel = 1;
    this.bgVelocity = 0;
    this.startLifeTransition(jungleRun.RUNNER_EXPOSION_DURATION);
    setTimeout(function(){
      jungleRun.endLifeTransition();
    }, TRANSITION_DURATION);
   },


   startLifeTransition: function(slowMotionDelay)
   {
      var CANVAS_TRANSITION_OPACITY = 0.05,
        SLOW_MOTION_RATE = 0.1;
      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;
      setTimeout(function(){
        jungleRun.setTimeRate(SLOW_MOTION_RATE);
        jungleRun.runner.visible = false;
      }, slowMotionDelay);
   },

   endLifeTransition: function()
   {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;
      jungleRun.reset();
      setTimeout(function(){
        jungleRun.setTimeRate(1.0);
        setTimeout(function(){
          jungleRun.runner.runAnimationRate = 0;
          jungleRun.playing = true;
        }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },

   reset: function()
   {
     jungleRun.score = 0;
     jungleRun.resetOffsets();
     jungleRun.resetRunner();
     jungleRun.makeAllSpritesVisible();
     jungleRun.canvas.style.opacity = 1.0;
     //this.highScore = jungleRun.setHighScore(this.totalScore, this.highScore);
     //jungleRun.totalScore = 0;
   },

   resetRunner: function()
   {
      this.runner.left = jungleRun.RUNNER_LEFT;
      this.runner.track = 4;
      this.runner.hOffset = 0;
      this.runner.visible = true;
      this.runner.exploding = false;
      this.runner.jumping = false;
      this.runner.top = this.calculatePlatformTop(4) - this.runner.height;
      this.runner.artist.cells = this.runnerCellsRight;
      this.runner.artist.cellIndex = 0;
   },

   resetOffsets: function()
   {
      this.bgVelocity = 0;
      this.backgroundOffset = 0;
      this.platformOffset = 0;
      this.spriteOffset = 0;
   },

   makeAllSpritesVisible: function()
   {
      for(var i=0; i < jungleRun.sprites.length; ++i)
      {
        jungleRun.sprites[i].visible = true;
      }
   },
   

   //Sounds 
   createAudioChannels: function()
   {
      var channel;
      for(var i=0; i < this.audioChannels.length; ++i)
      {
        channel = this.audioChannels[i];
        if(i !== 0)
        {
          channel.audio = document.createElement('audio');
          channel.audio.addEventListener('loadeddata', this.soundLoaded, false);
          channel.audio.src = this.audioSprites.currentSrc;
        }
        channel.audio.autobuffer = true;
      }
   },

   seekAudio: function(sound, audio)
   {
      try{
        audio.pause();
        audio.currentTime = sound.position;
      }
      catch(e)
      {
        if(console)
        {
          console.error("Cannot seek audio");
        }
     }
   },

   playAudio: function(audio, channel)
   {
      try{
        audio.play();
        channel.playing = true;
      }
      catch(e)
      {
        if(console)
        {
          console.error("Cannot play audio");
        }
      }
   },

   soundLoaded: function()
   {
      jungleRun.audioSpriteCountdown--;
      if(jungleRun.audioSpriteCountdown === 0)
      {
         if(!jungleRun.gameStarted && jungleRun.graphicsReady)         
         {
            jungleRun.startGame();
         }
      }
   },

  getFirstAvailableAudioChannel: function()
   {
      for(var i=0; i < this.audioChannels.length;++i)
      {
          if(!this.audioChannels[i].playing)
          {
            return this.audioChannels[i];
          }
      }
      return null;
   },

   playSound: function(sound)
   {
      var channel, audio;
      if(this.soundOn)
      {
        channel = this.getFirstAvailableAudioChannel();
        if(!channel)
        {
          if(console)
          {
            console.warn('All audio channels are busy. Cannot play the sound');
          }
        }
        else
        {
          audio = channel.audio;
          audio.volume = sound.volume;
          this.seekAudio(sound, audio);
          this.playAudio(audio, channel);

          setTimeout(function() {
            channel.playing = false;
            jungleRun.seekAudio(sound, audio);
          }, sound.duration);
        }
      }
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
          SOUNDTRACK_LENGTH = 103,
          timerID;
      timerID = setInterval(function(){
        if(jungleRun.musicElement.currentTime > SOUNDTRACK_LENGTH)
        {
          clearInterval(timerID); //Stop polling
          jungleRun.restartMusic();
        }
      }, POLL_INTERVAL);
   },

   restartMusic: function()
   {
      jungleRun.musicElement.pause();
      jungleRun.musicElement.currentTime = 0;
      jungleRun.startMusic();
   },

   startMusic: function()
   {
      var MUSIC_DELAY = 1000;
      setTimeout(function(){
        if(jungleRun.musicCheckboxElement.checked)
        {
          jungleRun.musicElement.play();
        }
        jungleRun.pollMusic();
      }, MUSIC_DELAY);
    },

    /* Mobile Device */

   getViewportSize: function () {
      return { 
        width: Math.max(document.documentElement.clientWidth ||
               window.innerWidth || 0),  
               
        height: Math.max(document.documentElement.clientHeight ||
                window.innerHeight || 0)
      };
   },

   detectMobile: function () {
      jungleRun.mobile = 'ontouchstart' in window;
   },

   resizeElement: function (element, w, h) {
      console.log(element);
      element.style.width  = w + 'px';
      element.style.height = h + 'px';
   },

   resizeElementsToFitScreen: function (arenaWidth, arenaHeight) {
      jungleRun.resizeElement(
         document.getElementById('arena'), 
         arenaWidth, arenaHeight);

      jungleRun.resizeElement(jungleRun.mobileWelcomeToast,
                              arenaWidth, arenaHeight);

      jungleRun.resizeElement(jungleRun.mobileStartToast,
                              arenaWidth, arenaHeight);
   },

   calculateArenaSize: function (viewportSize) {
      var DESKTOP_ARENA_WIDTH  = 800,  // Pixels
          DESKTOP_ARENA_HEIGHT = 520,  // Pixels
          arenaHeight,
          arenaWidth;

      arenaHeight = viewportSize.width * 
                    (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);

      if (arenaHeight < viewportSize.height) { // Height fits
         arenaWidth = viewportSize.width;      // Set width
      }
      else {                                   // Height does not fit
         arenaHeight = viewportSize.height;    // Recalculate height
         arenaWidth  = arenaHeight *           // Set width
                      (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
      }

      if (arenaWidth > DESKTOP_ARENA_WIDTH) {  // Too wide
         arenaWidth = DESKTOP_ARENA_WIDTH;     // Limit width
      } 

      if (arenaHeight > DESKTOP_ARENA_HEIGHT) { // Too tall
         arenaHeight = DESKTOP_ARENA_HEIGHT;    // Limit height
      }

      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },



   fitScreen: function () {
      var arenaSize = jungleRun.calculateArenaSize(
                         jungleRun.getViewportSize());

      jungleRun.resizeElementsToFitScreen(arenaSize.width, 
                                          arenaSize.height);
   },

   processRightTap: function () {
    console.log("Right Tap");
      if (jungleRun.runner.direction === jungleRun.LEFT ||
          jungleRun.bgVelocity === 0) {
         jungleRun.turnRight();
      }
      else {
         jungleRun.runner.jump();
      }
   },

   processLeftTap: function () {
        console.log("Left Tap");
      if (jungleRun.runner.direction === jungleRun.RIGHT) {
         jungleRun.turnLeft();
      }
      else {
         jungleRun.runner.jump();
      }
   },

   touchStart: function (e) {
      if (jungleRun.playing) {
         // Prevent players from inadvertently 
         // dragging the game canvas
         e.preventDefault(); 
      }
   },

   touchEnd: function (e) {
      var x = e.changedTouches[0].pageX;
console.log("touchEnd");
      if (jungleRun.playing) {
         if (x < jungleRun.canvas.width/2) {
            jungleRun.processLeftTap();
         }
         else if (x > jungleRun.canvas.width/2) {
            jungleRun.processRightTap();
         }

         // Prevent players from double
         // tapping to zoom into the canvas

         e.preventDefault(); 
      }
   },

   addTouchEventHandlers: function () {
      jungleRun.canvas.addEventListener(
         'touchstart', 
         jungleRun.touchStart
      );

      jungleRun.canvas.addEventListener(
         'touchend', 
         jungleRun.touchEnd
      );
   },

   initializeContextForMobileInstructions: function () {
      this.context.textAlign = 'center';
      this.context.textBaseline = 'middle';

      this.context.font = '26px fantasy';

      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'rgb(0,0,0)';

      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

   drawMobileDivider: function (cw, ch) {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stroke();
   },

   drawMobileInstructionsLeft: function (cw, ch, 
                                         topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';

      this.context.fillText('Tap on this side to:', 
         cw/4, ch/2 - topLineOffset);

      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';

      this.context.fillText('Turn around when running right', 
         cw/4, ch/2 - topLineOffset + 2*lineHeight);

      this.context.fillText('Jump when running left', 
         cw/4, ch/2 - topLineOffset + 3*lineHeight);
   },

   drawMobileInstructionsRight: function (cw, ch, 
                                          topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';
      this.context.fillStyle = 'yellow';

      this.context.fillText('Tap on this side to:', 
         3*cw/4, ch/2 - topLineOffset);

      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';

      this.context.fillText('Turn around when running left', 
         3*cw/4, ch/2 - topLineOffset + 2*lineHeight);

      this.context.fillText('Jump when running right', 
         3*cw/4, ch/2 - topLineOffset + 3*lineHeight);

      this.context.fillText('Start running', 
         3*cw/4, ch/2 - topLineOffset + 5*lineHeight);
   },

   drawMobileInstructions: function () {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();

      this.initializeContextForMobileInstructions();

      this.drawMobileDivider(cw, ch);

      this.drawMobileInstructionsLeft(cw, ch, 
                                      TOP_LINE_OFFSET, 
                                      LINE_HEIGHT);

      this.drawMobileInstructionsRight(cw, ch, 
                                       TOP_LINE_OFFSET, 
                                       LINE_HEIGHT);

      this.context.restore();
   },

   revealMobileStartToast: function () {
      jungleRun.fadeInElements(jungleRun.mobileStartToast);
   },
 
};


window.addEventListener('keydown', function(e){
  var key = e.keyCode;
  if((key === jungleRun.KEY_D && jungleRun.gamePaused === true)|| (key === jungleRun.KEY_LEFT && jungleRun.gamePaused === true)) 
  {
    jungleRun.turnLeft();
  }
  else if((key === jungleRun.KEY_K && jungleRun.gamePaused === true) || (key === jungleRun.KEY_RIGHT && jungleRun.gamePaused === true)) 
  {
    jungleRun.turnRight();
  }
  else if(key === jungleRun.KEY_P && jungleRun.gamePaused === true)
  {
    jungleRun.togglePaused();
  }
  else if(key === 74)
  {
    jungleRun.runner.jump();
  }
  else if(key === 38 && jungleRun.gameFinish === true)
  {
    jungleRun.win();
    jungleRun.fadeOutElements(jungleRun.gameCompleteElement, 5000);
    jungleRun.gameFinish = false;
    jungleRun.gamePaused = true;
  }
});

window.addEventListener('blur', function(e)
{
  jungleRun.windowHasFocus = false;
  if(!jungleRun.paused)
  {
    jungleRun.togglePaused(); //Pause the game
}});

//CLONKEENCOLE
window.addEventListener("beforeunload", function (e) {
    localStorage.setItem("highScore", jungleRun.highScore);
});
/*

*/


var jungleRun = new jungleRun();

// Mobile Device
jungleRun.showHowLink.addEventListener('click', function (e) {
      var FADE_DURATION = 1000;

      jungleRun.fadeOutElements(jungleRun.mobileWelcomeToast, 
                                FADE_DURATION);

      jungleRun.drawMobileInstructions();
      jungleRun.revealMobileStartToast();
      jungleRun.mobileInstructionsVisible = true;
});

jungleRun.mobileStartLink.addEventListener(
   'click', 

   function (e) {
      var FADE_DURATION = 1000;

      jungleRun.fadeOutElements(jungleRun.mobileStartToast, 
                                FADE_DURATION);

      jungleRun.mobileInstructionsVisible = false;
      jungleRun.playSound(jungleRun.coinSound);
      jungleRun.playing = true;
   }
);

jungleRun.welcomeStartLink.addEventListener(
   'click',

   function (e) {
      var FADE_DURATION = 1000;

      jungleRun.playSound(jungleRun.coinSound);
      jungleRun.fadeOutElements(jungleRun.mobileWelcomeToast, 
                                FADE_DURATION);
      jungleRun.playing = true;
   }
);

//Sound and music checkbox event handlers
jungleRun.musicCheckboxElement.addEventListener('change',
  function(e){
    jungleRun.musicOn = jungleRun.musicCheckboxElement.checked;
    if(jungleRun.musicOn){
      jungleRun.musicElement.play();
    }
    else{
      jungleRun.musicElement.pause();
    }
  });

jungleRun.soundCheckboxElement.addEventListener('change',
  function(e){
    jungleRun.soundOn = jungleRun.soundCheckboxElement.checked;
  });
jungleRun.initializeImages();
jungleRun.createSprites();
jungleRun.resetRunner();
jungleRun.createAudioChannels();

// Mobile Device

jungleRun.detectMobile();

if (jungleRun.mobile) {
   jungleRun.DEFAULT_RUNNING_SLOWLY_THRESHOLD = 30; // fps

   jungleRun.instructionsElement = 
      document.getElementById('jungleRun-mobile-instructions');

   jungleRun.addTouchEventHandlers();

   if (/android/i.test(navigator.userAgent)) {      
      jungleRun.coinSound.position = 4.8;     
      jungleRun.explosionSound.position = 2.8;
      jungleRun.pianoSound.position = 3.5;
      jungleRun.thudSound.position = 1.8;
      jungleRun.splashSound.position = 9.3;
      
   }
}

//jungleRun.fitScreen();
window.addEventListener("resize", jungleRun.fitScreen);
window.addEventListener("orientationchange", jungleRun.fitScreen);

